def conversie10(n, b):
    p = 1
    m = 0
    while n != 0:
        r = n % 10
        m += r * p
        p *= b
        n //= 10
    return m

if __name__ == "__main__":
    n = int(input("Dati numarul:"))
    b = int(input("Dati baza:"))
    print(conversie10(n ,b))